package com.example.SEI.demo.entity

    import jakarta.persistence.Entity
    import jakarta.persistence.GeneratedValue
    import jakarta.persistence.GenerationType
    import kotlinx.serialization.SerialName
    import kotlinx.serialization.Serializable



@Serializable
    data class UserData(
    @SerialName("userID") val userID: Int ,
    @SerialName("firstName") val firstName: String,
    @SerialName("lastName") val lastName: String,
    @SerialName("pin") val pin: Int,
    @SerialName("inGym") val inGym: Int
    ) {
    }

