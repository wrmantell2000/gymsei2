package com.example.SEI.demo.controller

//import com.example.SEI.demo.entity.FlowerData
import com.example.SEI.demo.entity.UserData
import com.example.SEI.demo.requests.NewGymUser
import com.example.SEI.demo.requests.UpdateUser
import org.apache.juli.logging.Log
import org.springframework.core.metrics.StartupStep.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/gym")
class ItemController (private val jdbcTemplate: JdbcTemplate) {

    @GetMapping("/users")
    fun getFlowerInfo(): MutableList<UserData> {
        val query = "SELECT * FROM GymUsers"
        return jdbcTemplate.query(query) { rs, _ ->
            UserData(
                userID = rs.getInt("UserID"),
                firstName = rs.getString("FirstName"),
                lastName = rs.getString("LastName"),
                inGym = rs.getInt("InGym"),
                    pin = rs.getInt("Pin"),
            )
        }
    }

    @GetMapping("/findbyid")
    fun findById(@RequestParam(name ="FirstName", required = true) userID: Int): MutableList<UserData> {
        val query = "SELECT * FROM GymUsers WHERE UserID = $userID"
        return jdbcTemplate.query(query) { rs, _ ->
            UserData(
                    userID = rs.getInt("UserID"),
                    firstName = rs.getString("FirstName"),
                    lastName = rs.getString("LastName"),
                    pin = rs.getInt("pin"),
                    inGym = rs.getInt("InGym")
            ) }
    }

   @PostMapping("/updateuser")
fun updateUser(@RequestBody updateUser: UpdateUser): Any? {

    when (updateUser.inGym) {
        0 -> {
            updateUser.inGym = 1
        }
        1 -> {
            updateUser.inGym = 0
        }
        else -> {
            return HttpStatus.BAD_REQUEST
        }
    }

    val query = "UPDATE GymUsers SET inGym = ? WHERE userID = ?"

    jdbcTemplate.update(query, updateUser.inGym, updateUser.userID)
   return ResponseEntity.ok()
}

@PostMapping("/newuser")
fun addNewUser(@RequestBody newUser: NewGymUser): Any? {
    val query = "INSERT INTO GymUsers(userID, FirstName, LastName, Pin, InGym) VALUES (?, ?, ?, ?, ?)"
    jdbcTemplate.update(query,newUser.userID, newUser.firstName, newUser.lastName, newUser.pin, newUser.inGym)
    return  HttpStatus.CREATED
}

@DeleteMapping("/removeuser")
fun removeItem(@RequestBody removeUser: UserData): Any? {
    val query = "DELETE FROM GymUsers WHERE userID = ?"
    jdbcTemplate.update(query, removeUser.userID)
    return HttpStatus.OK
}
}