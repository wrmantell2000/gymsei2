package com.example.SEI.demo.requests

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UpdateUser(
        @SerialName("userID")val userID: Int,
        @SerialName("firstName")val firstName: String,
        @SerialName("inGym")var inGym: Int,
        @SerialName("pin")var pin: Int
        )
