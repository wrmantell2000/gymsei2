package com.example.SEI.demo

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class DemoApplication

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args){
		setBannerMode(Banner.Mode.OFF)
	}
}

//@RestController
//class MyController {
//
//	@GetMapping("/hello")
//	fun hello(): String {
//		return "Hello, World!"
//	}
//}